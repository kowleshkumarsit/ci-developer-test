<html>
<head>
<title>CIBLOG</title>
<link rel="stylesheet" href="https://bootswatch.com/5/flatly/bootstrap.css"/>
<link rel="stylesheet" href="https://bootswatch.com/_assets/css/custom.min.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css"/>
</head>
<body>
<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-success">
      <div class="container">
        <a href="<?php echo base_url() ?>" class="navbar-brand">CI DEVELOPER TEST</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>location">Get Location</a>
            </li>
            
          </ul>
          <ul class="navbar-nav ms-md-auto">
          
          <?php if(!$this->session->userdata('logged_in')) : ?>
            
          <?php endif; ?>
          <?php if($this->session->userdata('logged_in')) : ?>
            <li ><a style="color:#fff;" href="<?php echo base_url(); ?>welcome/logout">Logout</a></li>
          <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
      <?php if($this->session->flashdata('user_loggedin')) : ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'?>
      <?php endif ?>
      <?php if($this->session->flashdata('user_loggedout')) : ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'?>
      <?php endif ?>
      <?php if($this->session->flashdata('login_failed')) : ?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'?>
      <?php endif ?>
      