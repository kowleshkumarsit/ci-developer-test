<?php $this->load->view('templates/header'); ?>
<div class="row">
        <div class="col-lg-12">
    			<form id="mapform"  method="post" class="mt-3">
    				<div class="input-group">
    				   <!-- <input type="text" name="desc" id="desc" class="form-control" placeholder="Enter a description..."> -->
    				   <input type="text" name="lat" id="lat" value="">
               <input type="text" name="lng" id="lng" value="">
               
    				   <span class="input-group-btn">
                  
                    <button class="btn btn-success ml-2" type="button" disabled="">Get a Location</button>
                  
    				   </span>
    				</div>
    			</form>
    			
    			<div class="mt-3" id="map_div">
    				<div style="height:450px;" id="map"></div> <!-- Map Div -->
    			</div>
        </div>
</div>
</div>
<script>
      function initMap() {
        var marker;
        var mapDiv = document.getElementById('map');
        var map = new google.maps.Map(mapDiv, {
          zoom: <?php if (isset($edit_page_data)) { echo '15'; } else { echo '12';} ?>,
          center: new google.maps.LatLng(<?php if (isset($edit_page_data)) { echo $edit_page_data->lat; } else {echo '22.75220152461084';} ?>, <?php if (isset($edit_page_data)) { echo $edit_page_data->lng; } else {echo '86.22488385937501';} ?>)
        });


        google.maps.event.addListener(map, 'click', function( event ){
         document.getElementById("lat").value = event.latLng.lat();
         document.getElementById("lng").value = event.latLng.lng();
        });

        map.addListener('click', function(e) {
            placeMarker(e.latLng, map);
        });

        function placeMarker(location,map) {
          if ( marker ) {
            marker.setPosition(location);
          } else {
            marker = new google.maps.Marker({
              position: location,
              map: map
            });
          }
          map.panTo(location);
        }

      }
	</script>
<script src="https://bootswatch.com/_vendor/jquery/dist/jquery.min.js"></script>
<script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

  <!-- You must first enter Your API Key on api-key.php file | application/views/api-key.php -->

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH1syq56YmwPzHiP1Ls7EwUQ7sGutSTCk&callback=initMap&libraries=places&v=weekly&language=en"></script>

  <!-- You must first enter Your API Key on api-key.php file | application/views/api-key.php -->


	<script type="text/javascript">
    

    $(document).ready(function () {
      $('button').on('click', function(){
        
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        console.log(lat);
        console.log(lng);
        
      
         $('#lat').html(lat);
         $('#lng').html(lng);
      
       })
      $('#map_div').on('click', function(){
        $('button').attr('disabled', false);
      })
    })  

    
    $('#mapform').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    });

	</script>

  </body>
</html>