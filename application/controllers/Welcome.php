<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['title'] = 'LOGIN';
		//$this->load->view('welcome_message');
        $this->load->view('templates/header');
        $this->load->view('welcome_message',$data);
        $this->load->view('templates/footer');
	}

	public function login(){
		//$data['title'] = 'Sign In';
		$data['title'] = 'LOGIN';
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('welcome_message', $data);
			$this->load->view('templates/footer');
		} else {
			
			// Get username
			$username = $this->input->post('username');
			// Get and encrypt the password
			$password = md5($this->input->post('password'));

			// Login user
			$result = $this->login_model->login($username, $password);

			if($result->num_rows() > 0){
				$data  = $result->row_array();
				$username  = $data['username'];
				$level = $data['level'];
				$user_data = array(
					'level' => $level,
					'username' => $username,
					'logged_in' => true
				);

				$this->session->set_userdata($user_data);

				// Set message
				$this->session->set_flashdata('user_loggedin', 'You are now logged in');

				if($level === '1') {
					redirect('Admin');
				} elseif($level === '2') {
					redirect('Customer');
				} else {
					show_404();
				}
			} else {
				// Set message
				$this->session->set_flashdata('login_failed', 'Invalid Credentials');
				//$this->load->view('templates/header');
				//$this->load->view('welcome_message', $data);
				//$this->load->view('templates/footer');
				redirect('welcome');
			}		
		}
	}

	// Log user out
	public function logout(){
		// Unset user data
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('level');
		$this->session->unset_userdata('username');

		// Set message
		$this->session->set_flashdata('user_loggedout', 'You are now logged out');

		redirect('welcome');
	}
}
