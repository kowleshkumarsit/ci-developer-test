<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {
	

	public function index($order_id) {

        $check = $this->order_model->get_order($order_id);
        if(empty($check)){
            $this->output->set_status_header(400);
            //show_400();
        }
        else{
		//$data        = array();
        $order_info  = $this->order_model->order_info_by_id($order_id);
        $ordernumber = $order_info['orderNumber'];
        $order_date = $order_info['orderDate'];
        $status = $order_info['status'];
        $customer_id = $order_info['customerNumber'];

        $order_details_info = $this->order_model->orderdetails_info_by_id($order_id);
        $order_amount = $this->order_model->order_amount_by_id($order_id);
        $amount=$order_amount['bill_amount'];

        $customer_info      = $this->order_model->customer_info_by_id($customer_id);
        //$payment_id  = $order_info->payment_id;
		$data = array(
            'order_id' => $ordernumber,
            'order_date' => $order_date,
            'status' => $status,
            'order_details'=>$order_details_info ,
            'bill_amount'=>$amount,
            'customer'=>$customer_info
        );
        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
        exit;
        //echo  json_encode($data);
        }
	}
    
}