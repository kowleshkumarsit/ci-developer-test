<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if($this->session->userdata('logged_in') !== TRUE) {
			redirect('welcome');
		}
	}

	public function index() {
		$data['username']=$this->session->userdata('username');
			$this->load->view('templates/header');
			$this->load->view('customer/index', $data);
			$this->load->view('templates/footer');
		
	}
}