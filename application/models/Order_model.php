<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function __construct()
	{
        $this->load->database();  
	}

    public function get_order($order_id)
    {
        
        $query = $this->db->get_where('orders',array('orderNumber' =>$order_id));
        return $query->row_array();
    }

    public function order_info_by_id($order_id)
    {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where('orderNumber', $order_id);
        $result = $this->db->get();
        return $result->row_array();
    }
    
    public function orderdetails_info_by_id($order_id)
    {
        $this->db->select('productName AS product,productLine AS product_line,priceEach AS unit_price,quantityOrdered AS qty,(priceEach * quantityOrdered) AS line_total');
        $this->db->from('orderdetails');
        $this->db->join('products', 'products.productCode = orderdetails.productCode');
        $this->db->where('orderdetails.orderNumber', $order_id);
        $result = $this->db->get();
        return $result->result_array();
    }
    public function order_amount_by_id($order_id)
    {
        $this->db->select('SUM(priceEach * quantityOrdered) AS bill_amount');
        $this->db->from('orderdetails');
        $this->db->where('orderNumber', $order_id);
        $result = $this->db->get();
        return $result->row_array();
    }
    public function customer_info_by_id($customer_id)
    {
        $this->db->select('contactFirstName As first_name,contactLastName AS last_name,phone,country AS country_code');
        $this->db->from('customers');
        $this->db->where('customerNumber', $customer_id);
        $result = $this->db->get();
        return $result->row_array();
    }

}